const Joi = require('joi');
const express = require('express');
const app = express();

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
};
app.use(logger);

const router = require('./router');
// Uncomment this if you want to enable the router middleware
// app.use(router);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const products = ["Apple", "Redmi", "One Plus One"];
const orders = [
    { id: 1, paid: false, user_id: 1 },
    { id: 2, paid: false, user_id: 1 },
];

app.get('/', (req, res) => {
    res.send('Hello World');
});

app.get('/error', (req, res) => {
    error;
});

app.get('/products', (req, res) => {
    res.json(products);
})

app.get('/orders', (req, res) => {
    res.json(orders);
});

app.get('/orders/:id', (req, res) => {
    const order = orders.find(o => o.id == req.params.id);
    if (!order) return res.status(404).send("Order not found");
    res.json(order);
});

app.post('/products', (req, res) => {
    const result = validateProduct(req.body);
    const { error } = validateProduct(req.body);

    if (error) return res.status(400).send(error.details[0].message);

    products.push(req.body.name);
    res.json(result);
});

app.post('/orders', (req, res) => {
    const result = validateOrder(req.body);
    const { error } = validateOrder(req.body);

    if (error) return res.status(400).send(error.details[0].message);

    const order = { id: orders.length + 1, paid: req.body.paid, user_id: req.body.user_id };
    orders.push(order);
    res.json(result);

});

app.put('/orders/:id', (req, res) => {
    const order = orders.find(o => o.id == req.params.id);
    if (!order) res.status(404).send("Order not found");

    const { error } = validateOrder(req.body);

    if (error) {
        res.status(400).send(error.details[0].message);
        return;
    }

    order.paid = req.body.paid;
    order.user_id = req.body.user_id;

    res.json(order);
});

app.delete('/orders/:id', (req, res) => {
    const order = orders.find(c => c.id == req.params.id);
    if (!order) return res.status(404).send("Order not found");

    const index = orders.indexOf(order);
    orders.splice(index, 1);

    res.send(order);
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'failed',
        errors: err.message
    });
});

app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
        status: 'failed',
        errors: 'Path not found'
    });
});

// Port
const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`App is listening on port ${port}`));

function validateProduct(product) {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .required()
    });

    const result = schema.validate(product);
    console.log(result);

    return result;
}

function validateOrder(order) {
    const schema = Joi.object({
        paid: Joi.boolean()
            .required(),
        user_id: Joi.number()
            .required()
    });

    const result = schema.validate(order);
    console.log(result);

    return result;
}