const express = require('express');
const Joi = require('joi');
const app = express();

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '../../'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const router = require('./router');
app.use(router);

const users = [
    { id: 1, email: "testing@gmail.com", password: "password" },
];

app.get('/api/users', (req, res) => {
    res.json(users);
});

app.post('/api/users', (req, res) => {
    const existingUser = users.find((user) => {
        return user.email == req.body.email
    });

    if (existingUser == null) {
        const result = validateUser(req.body);
        const { error } = validateUser(req.body);

        if (error) return res.status(400).send(error.details[0].message);

        const user = { id: users.length + 1, email: req.body.email, password: req.body.password };
        users.push(user);
        res.json(result);
    } else return res.status(400).json({
        status: 'failed',
        errors: 'email has been used'
    });
});

app.post('/api/login', (req, res) => {
    const { error } = validateUser(req.body);

    if (error) return res.status(400).send(error.details[0].message);

    const email = req.body.email;
    const password = req.body.password;

    const existingUser = users.find((user) => {
        return user.email == email;
    });

    console.log(existingUser.email);

    if (existingUser != null) {
        if (existingUser.email == email) {
            if (existingUser.password == password) return res.status(200).json({
                status: 'success',
                message: 'login success'
            });
            else return res.status(400).json({
                status: 'failed',
                errors: 'incorrect password'
            });
        }
    } else return res.status(400).json({
        status: 'failed',
        errors: 'user not found'
    });
});

// 500 Error Handler
app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'failed',
        errors: err.message
    });
});

// 404 Error Handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
        status: 'failed',
        errors: 'Path not found'
    });
});

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`App is listening on port ${port}`));

function validateUser(user) {
    const schema = Joi.object({
        email: Joi.string()
            .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
            .required(),
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required()
    });

    const result = schema.validate(user);
    console.log(result);

    return result;
}