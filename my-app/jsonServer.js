const http = require('http');

function onRequest(request, response) {
    response.writeHead(200, { "Content-Type": "application/json" });
    const data = {
        name: "Alga Vania",
        age: 16
    };
    response.end(JSON.stringify(data));
};

http.createServer(onRequest).listen(5001);